import React from 'react';
import Screen from '../../component/screen';
import Container from '@material-ui/core/Container';
function Masuk() {
  return (
    <React.Fragment>
      <Container maxWidth="xs">
        <Screen />
      </Container>
    </React.Fragment>
  );
}

export default Masuk;
