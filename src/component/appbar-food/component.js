import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import LocationOn from '@material-ui/icons/LocationOn';
import { IconButton, Container } from '@material-ui/core';
import Back from '../../asset/back.png';
import { withRouter } from 'react-router-dom';

function Food(props) {
  const { classes } = props;
  const handleBack = () => {
    props.history.push('/');
  };
  return (
    <React.Fragment>
        <Box display="flex" justifyContent="center">
          <AppBar className={classes.AppBar} position="static">
            <Toolbar>
              <IconButton>
                <img src={Back} onClick={handleBack} className={classes.Back} />
              </IconButton>
            </Toolbar>
            <Typography className={classes.Antar}>Antar ke</Typography>
            <LocationOn className={classes.Lokasi} />
            <Typography className={classes.Hotel}>
              <strong>HOTEL DAFAM SEMARANG</strong>
            </Typography>
          </AppBar>
        </Box>
    </React.Fragment>
  );
}

export default withRouter(Food);
