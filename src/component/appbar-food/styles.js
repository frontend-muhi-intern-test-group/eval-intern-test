const Styles = {
  AppBar: {
    width: 411,
    color: 'black',
    backgroundColor: 'white',
    height: 79,
    position: 'fixed'
  },
  Back: {
    width: '30px',
    height: '30px',
    marginLeft: '-40px',
    color: '#5E52AD',
    marginTop: '15px',
    paddingLeft: '30px'
  },
  Antar: {
    marginLeft: '65px',
    marginTop: '-45px',
    height: '50px',
    fontSize: '12px'
  },
  Lokasi: {
    color: '#F56363',
    marginLeft: '57px',
    width: '30px',
    height: '20px',
    marginTop: '-30px'
  },
  Hotel: {
    fontSize: '15px',
    marginLeft: '85px',
    marginTop: '-20px'
  }
};

export default Styles;
