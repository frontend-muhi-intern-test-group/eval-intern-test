const Styles = {
  Gambar: {
    marginLeft: 5,
    marginTop: '90px',
    width: 385,
    height: 178
  },
  LabelPromo: {
    background: '#109666',
    borderRadius: '5px',
    marginLeft: '10px',
    marginTop: '-70px'
  },
  Promo: {
    fontSize: '12px',
    color: 'white',
    textTransform: 'none'
  },
  Terfavorit: {
    marginTop: '5px',
    marginLeft: '10px',
    position: 'absolute'
  }
};

export default Styles;
