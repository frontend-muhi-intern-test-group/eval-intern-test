import React from 'react';
import Gambar from '../../asset/makanan.png';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

function Menuu(props) {
  const { classes } = props;
  return (
    <React.Fragment>
      <img src={Gambar} className={classes.Gambar} />
      <Button className={classes.LabelPromo}>
        <Typography className={classes.Promo}>
          <strong>Promo</strong>
        </Typography>
      </Button>
      <Typography className={classes.Terfavorit}>
        <strong>Makanan Terfavorit</strong>
      </Typography>
    </React.Fragment>
  );
}
export default Menuu;
