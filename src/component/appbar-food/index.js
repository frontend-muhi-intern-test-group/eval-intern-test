import Component from './component';
import { withStyles } from '@material-ui/core/styles';
import styles from './styles';

const Styled = withStyles(styles)(Component);
export default Styled;
