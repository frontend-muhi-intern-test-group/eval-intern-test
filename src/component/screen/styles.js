const Styles = {
  Image: {
    marginTop:40,
    marginLeft: '50px',
    textAlign: 'center',
    position: 'static',
    height: 220,
    width: 250
  },
  Selamat: {
    fontSize: 24,
    color: '#5E52AD',
    marginTop: 15,
    marginLeft: '90px'
  },
  Masuk: {
    marginTop: 25,
    fontSize: '12px',
    marginLeft: '65px',
    color: '#8B8B8B'
  },
  Btn: {
    background: '#5E52AD',
    width: '170%',
    height: '49px',
    borderRadius: '50px',
    marginTop: 50,
    marginLeft: '37px'
  },
  Dengan: {
    fontSize: 12,
    marginLeft: '40px',
    marginTop: 50,
    color: '#8B8B8B'

  },
  Penggunaan: {
    fontSize: 12,
    marginTop: 10,
    marginLeft: '48px',
    textDecoration: 'underline',
    color: '#8B8B8B'

  },
};

export default Styles;
