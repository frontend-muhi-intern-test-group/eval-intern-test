const Styles = {
  Img: {
    height: '100px',
    width: '100px',
    marginLeft: '-10px',
    marginTop: '30px'
  },
  Nama: {
    marginLeft: '20px',
    paddingTop: '10px',
    fontSize: '14px',
    position: 'absolute'
  },
  Jenis: {
    marginLeft: '20px',
    fontSize: '12px',
    position: 'absolute',
    marginTop: '40px'
  },
  Harga: {
    marginTop: '95px',
    marginLeft: '20px',
    position: 'absolute'
  },
  GridBtn: {
    marginLeft: '120px',
    marginTop: '50px',
    position: 'absolute'
  },
  Tambah: {
    fontSize: '12px',
    color: 'white',
    textTransform: 'none'
  },
  Number: {
    marginLeft: '5px',
    marginTop: '15px',
    fontSize: '10px'
  },
  BtnTambah: {
    marginLeft: '5px',
    marginTop: '10px'
  },
  BtnKurang: {
    marginLeft: '5px',
    marginTop: '10px'
  }
};

export default Styles;
