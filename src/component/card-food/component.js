import React, { useState } from 'react';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Ceklis from '../../asset/ceklis.png';

function Cardmenu(props) {
  const [number, setNumber] = useState(0);
  const { nama, harga, jenis, image } = props;
  const handlePlus = () => {
    setNumber(number + 1);
  };
  const handleMinus = () => {
    setNumber(number - 1);
  };
  const handleTambah = () => {
    setNumber(number + 1);
  };
  const hidenButton = () => {
    if (number == 0) {
      return {
        backgroundColor: '#5E52AD'
      };
    }
    if (number !== 0) {
      return {
        display: 'none'
      };
    }
  };

  const hideCount = () => {
    if (number == 0) {
      return {
        display: 'none'
      };
    }
    if (number !== 0) {
      return {
        display: 'flex',
        color: 'red'
      };
    }
  };
  return (
    <React.Fragment>
      <CardMedia
        style={{
          height: 100,
          width: 100,
          marginTop: '40px',
          marginLeft: '0px'
        }}
        image={image}
      />
      <Typography
        style={{
          marginLeft: '140px',
          marginTop: '-100px',
          fontSize: '18px'
        }}>
        <strong>{nama}</strong>
      </Typography>
      <Typography style={{ marginLeft: '140px', fontSize: '12px' }}>
        {jenis}
      </Typography>
      <Typography style={{ marginTop: '35px', marginLeft: '140px' }}>
        {harga}
      </Typography>
      <Grid style={{ marginLeft: '250px', marginTop: -10 }}>
        <Button onClick={() => handleTambah()} style={hidenButton()}>
          <Typography
            style={{
              fontSize: '12px',
              color: 'white',
              marginTop: '0px',
              textTransform: 'none'
            }}>
            Tambah
          </Typography>
        </Button>
        <Grid style={hideCount()} container spacing={0}>
          <Button
            onClick={() => handleMinus()}
            style={{
              marginLeft: '-50px',
              marginTop: '-40px',
              boxShadow: '0px 1px 4px rgba(109, 96, 96, 0.25)',
              width: '10px',
              height: '20px'
            }}>
            <Typography style={{ marginLeft: -20, marginTop: -7 }}>
              {' '}
              -{' '}
            </Typography>
          </Button>
          <h3 style={{ marginLeft: '-15px', marginTop: '-40px' }}>{number}</h3>
          <Button
            onClick={() => handlePlus()}
            style={{
              marginLeft: '-17px',
              marginTop: '-40px',
              boxShadow: '0px 1px 4px rgba(109, 96, 96, 0.25)',
              width: '10px',
              height: '20px'
            }}>
            <Typography style={{ marginLeft: 20, marginTop: -7 }}>
              {' '}
              +{' '}
            </Typography>
          </Button>
          <img
            src={Ceklis}
            style={{ marginTop: -43, height: 28, marginLeft: -145 }}
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
export default Cardmenu;
