import React from 'react';
import Gambar from '../../asset/pictures.png';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import CssBaseline from '@material-ui/core/CssBaseline'
function Screen(props) {
  const { classes } = props;
  return (  
  <React.Fragment>
      <CssBaseline />
      <Container
        maxWidth="xs"
        style={{
          height: "100vh",
          border: '1px solid #e4e4e4'
        }}
      >
      <Grid container pacing={0}>
        <Grid item={12} style={{ textAlign: 'center' }}>
          <img src={Gambar} className={classes.Image} />
        </Grid>
        <Grid item={12} style={{ textAlign: 'center' }}>
          <Typography className={classes.Selamat}>
            <strong>Selamat Datang</strong>
          </Typography>
        </Grid>
        <Grid item={12} style={{ marginLeft: '-20px' }}>
          <Typography className={classes.Masuk}>
            Masuk untuk menikmati makanan yang tersedia
          </Typography>
        </Grid>
        <Grid item={12} style={{ textAlign: 'center' }}>
          <Button
            className={classes.Btn}
            onClick={() => {
              props.history.push('/food');
            }}>
            <Grid item={12} style={{ textAlign: 'center' }}>
              <Typography
                style={{
                  color: 'white',
                  fontSize: '18px',
                  textTransform: 'none'
                }}>
                Masuk Disini
              </Typography>
            </Grid>
          </Button>
        </Grid>
        <Grid item={12} style={{ marginLeft: '-5' }}>
          <Typography className={classes.Dengan}>
            Dengan masuk dan mendaftar, Anda menyetujui {' '}
          </Typography>
        </Grid>
        <Grid item={12} style={{ textAlign: 'center' }}>
          <Typography className={classes.Penggunaan}>
          <strong>Syarat </strong><strong>Penggunaan</strong> dan <strong>Kebijakan privasi</strong>{' '}
          </Typography>
        </Grid>
      </Grid>
      </Container>
    </React.Fragment>
  );
      }
export default withRouter(Screen);

