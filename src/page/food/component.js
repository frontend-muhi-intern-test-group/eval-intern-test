import React from 'react';
import Food from '../../component/appbar-food';
import Container from '@material-ui/core/Container';
import Data from '../../data/data-dummy';
import Card from '../../component/card-food';
import Menuu from '../../component/menu';
function Foods() {
  return (
    <React.Fragment>
      <Container maxWidth="xs">
        <Food />
        <Menuu />
        {Data.map(item => {
          return (
            <Card
              nama={item.nama}
              jenis={item.jenis}
              harga={item.harga}
              image={item.image}
            />
          );
        })}
      </Container>
    </React.Fragment>
  );
}

export default Foods;
