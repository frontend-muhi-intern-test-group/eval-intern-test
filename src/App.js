import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Masuk from './page/masuk';
import Food from './page/food';

function App() {
  return (
    <React.Fragment>
      <Switch>
        <Route path="/" exact component={Masuk} />
        <Route path="/food" exact component={Food} />
      </Switch>
    </React.Fragment>
  );
}
export default App;
